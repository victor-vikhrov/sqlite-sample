package com.eh.recyclerlistsample;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

/**
 * Created by IT SCHOOL on 14.02.2018.
 */
public class NoteModel {

    private int id;
    private String title;
    private String text;
    private int categoryId;
    //private Date date; SimpleDateFormat "hh:mm dd MMMM"
    private String date; // 10:30 2 февраля

    public NoteModel(String title, String text, String date, int categoryId) {
        this.title = title;
        this.text = text;
        this.date = date;
        this.categoryId = categoryId;
    }

    public NoteModel() {

    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setDate(long timestamp) {
        Date date = new Date(timestamp);
        SimpleDateFormat dateFormat = new SimpleDateFormat("dd MMMM HH:mm", new Locale("RU"));
        this.date = dateFormat.format(date);
    }

    public int getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(int categoryId) {
        this.categoryId = categoryId;
    }
}
