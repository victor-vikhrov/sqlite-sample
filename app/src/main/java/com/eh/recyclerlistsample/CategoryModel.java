package com.eh.recyclerlistsample;

/**
 * Created by vvikhrov on 26.05.18.
 */

public class CategoryModel {
    private int id;
    private String name;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
